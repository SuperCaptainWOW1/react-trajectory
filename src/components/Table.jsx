import { Component } from "react";
import TableCourse from "./TableCourse";
import "./Table.css";

export default class Table extends Component {
  constructor() {
    super();

    this.state = {
      data: [
        {
          id: 1,
          title: "Математика",
          weight: 10,
          isTarget: false,

          course: 1,
          semester: 2,

          connectedTo: 3,
        },
        {
          id: 2,
          title: "Программирование",
          weight: 20,
          isTarget: false,

          course: 3,
          semester: 1,

          connectedTo: 2,
        },
        {
          id: 3,
          title: "Рисование",
          weight: 5,
          isTarget: false,

          course: 2,
          semester: 1,

          connectedTo: 2,
        },
        {
          id: 4,
          title: "Английский",
          weight: 20,
          isTarget: false,

          course: 2,
          semester: 1,

          connectedTo: 3,
        },
      ],
    };
  }

  render() {
    return (
      <div className="table">
        {[1, 2, 3, 4].map((course) => (
          <TableCourse
            key={course}
            courseNumber={course}
            data={this.state.data.filter((node) => node.course === course)}
          />
        ))}
      </div>
    );
  }
}
