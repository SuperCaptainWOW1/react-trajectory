import { Component } from "react";
import NodeItem from "./NodeItem";

export default class TableCourse extends Component {
  constructor(props) {
    super(props);

    this.state = {
      courseWeight: 0,
      semesterFirstWeight: 0,
      semesterSecondWeight: 0,
    };
  }

  componentDidMount() {
    this.recalculateWeights();
  }

  recalculateWeights() {
    this.setState({
      courseWeight: this.props.data.reduce((prev, cur) => prev + cur.weight, 0),
      semesterFirstWeight: this.props.data
        .filter((node) => node.semester === 1)
        .reduce((prev, cur) => prev + cur.weight, 0),
      semesterSecondWeight: this.props.data
        .filter((node) => node.semester === 2)
        .reduce((prev, cur) => prev + cur.weight, 0),
    });
  }

  render() {
    return (
      <div key={this.props.courseNumber} className="table__course">
        <div className="table__course__info">
          <p className="table__course__info__title">
            {this.props.courseNumber} курс
          </p>
          <p className="table__course__info__weight">
            {this.state.courseWeight}
          </p>
        </div>

        <div className="table__course__semesters">
          <div className="table__course__semesters__item">
            <div className="table__course__semesters__item__info">
              <p className="table__course__semesters__item__info__title">
                1 семестр
              </p>
              <p className="table__course__semesters__item__info__weight">
                {this.state.semesterFirstWeight}
              </p>
            </div>

            <div className="table__semesters__item__content">
              {this.props.data
                .filter((node) => node.semester === 1)
                .map((node) => (
                  <NodeItem
                    key={node.id}
                    title={node.title}
                    weight={node.weight}
                    isTarget={node.isTarget}
                  />
                ))}
            </div>
          </div>

          <div className="table__course__semesters__item">
            <div className="table__course__semesters__item__info">
              <p className="table__course__semesters__item__info__title">
                2 семестр
              </p>
              <p className="table__course__semesters__item__info__weight">
                {this.state.semesterSecondWeight}
              </p>
            </div>

            <div className="table__semesters__item__content">
              {this.props.data
                .filter((node) => node.semester === 2)
                .map((node) => (
                  <NodeItem
                    key={node.id}
                    title={node.title}
                    weight={node.weight}
                    isTarget={node.isTarget}
                  />
                ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
