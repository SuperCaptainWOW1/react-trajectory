import "./NodeItem.css";

export default function NodeItem({ title, weight, isTarget }) {
  return (
    <div className={`node-item ${isTarget && "is-target"}`}>
      <p className="title">{title}</p>
      <p className="weight">{weight}</p>
    </div>
  );
}
