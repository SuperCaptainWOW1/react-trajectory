import React from "react";

// import NodeItem from "./components/NodeItem";
import Table from "./components/Table";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        {/* <NodeItem title="Математика" weight="10"></NodeItem> */}
        <Table />
      </div>
    );
  }
}

export default App;
